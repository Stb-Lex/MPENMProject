import sys
import pickle

if(len(sys.argv) == 1):
    print('Function:')
    print('Merge a set of element set together')
    print('Usage:')
    print('mergeset.py outfilename infilename0 infilename1 infilename2 ...')
    quit()

elemset = []

for i in range(2, len(sys.argv)):
    print('Loading element set', sys.argv[i])

    data = pickle.load(open(sys.argv[i], 'rb'))
    print(len(data), 'elements found')

    for j in range(0, len(data)):
        elemset.append(data[j])


print('Saving final element set')
pickle.dump(elemset, open(sys.argv[1], 'wb'))
print(len(elemset), 'elements have been saved')

