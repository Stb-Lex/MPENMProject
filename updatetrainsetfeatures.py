import sys
from common import computeFeatures
import pickle

inputfile = sys.argv[1]
outputfile = ''
if(len(sys.argv) == 3):
    outputfile = sys.argv[2]
else:
    outputfile = inputfile

print('Updating features from set', inputfile)
print('Saving as', outputfile)


print('Loading trainset')
elemset = pickle.load(open(inputfile, 'rb'))
print('Done')

print('Updating...')
for elem in elemset:
    elem.features = computeFeatures(elem.snapshot, elem.regionprop, elem.mask)

print('Done')

print('Saving...')
pickle.dump(elemset, open(outputfile, 'wb'))
print('Done')


