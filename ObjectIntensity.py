import skimage.io
from skimage.filters import threshold_local
from skimage.measure import regionprops, label, moments_hu
from skimage.morphology import opening, disk, square, dilation, erosion
import skimage.measure
import numpy as np
from common import threshold
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import pickle
import sys
from scipy.stats import multivariate_normal
import matplotlib.mlab as mlab

img = skimage.io.imread('data\sample2_final_grey_image767.tif')
binary_adaptive = threshold(img)


testObj = 5427 #S-form för 5427?? Vissa fungerar bra



labels = label(binary_adaptive, background=0)
blobs = regionprops(labels)
store_img_final = np.zeros(img.shape)
for testObj in range(1,len(blobs)):
    ind = blobs[testObj]['coords']
    rowStore = []
    colStore = []
    for i in range(0,len(ind)):
        rowStore.append(ind[i][0])
        colStore.append(ind[i][1])

    meanInt = np.mean(img[rowStore,colStore])
    store_img_final[rowStore,colStore] = 100/(meanInt+1)*binary_adaptive[rowStore,colStore]

store_img_final[np.where(store_img_final>2)] = 2
plt.imshow(store_img_final, cmap='gray')
plt.show()
