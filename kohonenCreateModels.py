import numpy as np
import pickle
import sys
import random
import skimage.measure
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
from common import getFeatureSize
from common import getNModels
from element import Element


"""
See main function for parameters

kohonene20x20(...) takes data, number of dimensions, number of samples from the class we want to
identify (given after each other, then there should be some non-pore samples given) and some
parameters. It creates a network with 20x20 outputs.

"""
def kohonen20x20(data, dim, class_one_ends, eta_zero=0.1, sigma_zero=30, eta_c=0.01, sigma_c=0.9):
    weights = np.zeros(20*20*dim)
    inpt = np.zeros(len(data))

    n_inpt = int(len(data)//dim)

    eta = eta_zero
    sigma = sigma_zero

    tau = 300.0
    
    random.seed()

    for i in range(len(weights)):
        weights[i] = random.random()



    inpt = np.zeros(len(data))
    for i in range(len(inpt)):
        inpt[i] = data[i]
    
    means = np.zeros(dim)
    stdevs = np.zeros(dim)

    #normalize the inputs
    current = np.zeros(n_inpt)
    for j in range(dim):
        for i in range(n_inpt):
            current[i] = inpt[i*dim + j]
        mean = np.mean(current)
        stdev = np.sqrt(np.ma.var(current))
        means[j] = mean
        stdevs[j] = stdev
        for i in range(n_inpt):
            inpt[i*dim + j] = (inpt[i*dim + j] - mean)/stdev

    #ordering phase

    for t in range(1000):
        j = random.randrange(0,n_inpt)

        largest = -10000.0   #som number much smaller than relevant numbers

        i0=0
        for i in range(400):
            buffer = 0.0
            for k in range(dim):
                buffer += weights[i*dim + k]*inpt[j*dim + k]
            if (buffer > largest):
                largest = buffer
                i0 = i

        #update weights

        for k in range(dim):
            for i in range(400):
                weights[i*dim + k] += eta*np.exp(-((i//20-i0//20)*(i//20-i0//20) + (i%20-i0%20)*(i%20-i0%20))/(2.0*sigma*sigma))*(inpt[j*dim + k] - weights[i*dim + k])


        eta = eta_zero*np.exp(-t/tau)
        sigma = sigma_zero*np.exp(-t/tau)


    print("Ordering phase done \n")
    
    #converging phase

    eta = eta_c
    sigma = sigma_c

    for t in range(20000):
        if (t%100==0):
            print(t/200,'%')
        j = random.randrange(0,n_inpt)
        
        largest = -10000.0
        i0=0

        for i in range(400):
            buffer = 0.0
            for k in range(dim):
                buffer += weights[i*dim + k]*inpt[j*dim + k]
            if (buffer > largest):
                largest = buffer
                i0 = i

        for k in range(dim):
            for i in range(400):
                weights[i*dim + k] += eta*np.exp(-((i//20-i0//20)*(i//20-i0//20) + (i%20-i0%20)*(i%20-i0%20))/(2.0*sigma*sigma))*(inpt[j*dim + k] - weights[i*dim + k])

    print("Converging phase done \n")

    
    classifications = np.zeros(n_inpt*2)
    
    i0=0
    for j in range(n_inpt):
        largest = -10000.0
        for i in range(400):
            buffer = 0.0
            for k in range(dim):
                buffer += weights[i*dim + k]*inpt[j*dim + k]
            if (buffer > largest):
                largest = buffer
                i0 = i
        classifications[j*2] = int(i0//20)
        classifications[j*2+1] = int(i0%20)


    return weights,dim,means,stdevs


def main(argv):
    #Training set pore elements
    elemsetfilename = 'p'
    #Training set non-pore elements
    elemsetNonPoresFilename = 'np'
    #first part of the model file names. They are loaded as 'modelsfilename'+ a number + '.bin'
    modelsfile = 'model'
    

    print('Loading pore training set')
    elemset = pickle.load(open(elemsetfilename, 'rb'))
    print('Done')

    print('Loading non-pore training set')
    elemsetNonPores = pickle.load(open(elemsetNonPoresFilename, 'rb'))
    print('Done')


    X = np.zeros([len(elemset),getFeatureSize()])
    for i in range(len(elemset)):
        X[i,:] = elemset[i].features

    X2 = np.zeros([len(elemsetNonPores),getFeatureSize()])
    for i in range(len(elemsetNonPores)):
        X2[i,:] = elemsetNonPores[i].features
    
    
    print('Rearranging data to fit Kohonen function requirements')

    Y = np.zeros((len(elemset)+len(elemsetNonPores))*getFeatureSize())
    for i in range(len(elemset)):
        for j in range(getFeatureSize()):
            Y[i*getFeatureSize()+j]=X[i,j]
        
    lenElemset = len(elemset)

    
    for i in range(len(elemsetNonPores)):
        for j in range(getFeatureSize()):
            Y[(lenElemset+i)*getFeatureSize() + j] = X2[i,j]
    print('Done')

    print('Number of pore elements: ',lenElemset)
    print('Number of non-pore elements: ',len(elemsetNonPores))

    print('Number of models to create: ', getNModels())


    print('Training model (Kohonen)')

    for l in range(getNModels()):
        model = kohonen20x20(Y,getFeatureSize(),lenElemset,0.1,30,0.01,0.9)
        fileOutModel = modelsfile + str(l) +'.bin'
        print('    saving results for run',l)
        pickle.dump(model,open(fileOutModel,'wb'))
    print('Done')
    
    
if __name__ == "__main__":
    main(sys.argv)
