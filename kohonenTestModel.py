import skimage.io
from skimage.filters import threshold_local, gabor, gaussian
from skimage.measure import regionprops, label, moments_hu
from skimage.morphology import opening, disk, dilation, binary_dilation
from common import threshold, computeFeatures,getNModels,getFeatureSize
import skimage.measure
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import pickle
import sys
#Script to classify all blobs identified in an image

#INPUT FILES---------------------------------
inputimage = 'sample2_final_grey_image173.tif'
poreTrainingSet = 'p'
nonPoreTrainingSet = 'np'

#first part of the model file names. They are loaded as 'modelsfilename'+ a number + '.bin'
modelsfile = 'model'



#OUTPUT FILE------------------------------
#For fast plotting
resultsFile = 'final_results.bin'

     



#PARAMETERS---------------------------------
printProgress = True

#Training set results for a certain set of models need only to be
#computed once, and can then be used for different images
doPrepareModels = True

#Number of objects to consider, set to zero or less to consider all of them
nTimes = 100 

#Percentage of models classifying an object needed to consider it a pore
percentage = 0.08

#Number of models used is regulated in common.py






#sorting the input according to network's classification
def kohonen20x20predict(model, data):
    weights = model[0]
    dim = model[1]
    means = model[2]
    stdevs = model[3]
    
    inpt = np.zeros(len(data))
    for i in range(len(inpt)):
        inpt[i] = data[i]

    n_inpt = int(len(data)//dim)


    for j in range(dim):
        for i in range(n_inpt):
            inpt[i*dim + j] = (inpt[i*dim + j] - means[j])/stdevs[j]
    
    classifications = np.zeros(n_inpt*2)
    
    i0=0
    for j in range(n_inpt):
        largest = -10000.0
        for i in range(400):
            buffer = 0.0
            for k in range(dim):
                buffer += weights[i*dim + k]*inpt[j*dim + k]
            if (buffer > largest):
                largest = buffer
                i0 = i
        classifications[j*2] = int(i0//20)
        classifications[j*2+1] = int(i0%20)

    return classifications

def prepareModels():
    allCoords = []

    print('    Loading pore training set')
    elemset = pickle.load(open(poreTrainingSet, 'rb'))
    print('    Done')

    print('    Loading non-pore training set')
    elemsetNonPores = pickle.load(open(nonPoreTrainingSet, 'rb'))
    print('    Done')

    X = np.zeros([len(elemset),getFeatureSize()])
    for i in range(len(elemset)):
        X[i,:] = elemset[i].features

    
    X2 = np.zeros([len(elemsetNonPores),getFeatureSize()])
    for i in range(len(elemsetNonPores)):
        X2[i,:] = elemsetNonPores[i].features
    
    Y = np.zeros((len(elemset)+len(elemsetNonPores))*getFeatureSize())
    for i in range(len(elemset)):
        for j in range(getFeatureSize()):
            Y[i*getFeatureSize()+j]=X[i,j]
    
    for l in range(getNModels()):
        model = models[l]
        
        #training set
        classif = kohonen20x20predict(model, Y)

        x=[]
        y=[]
        for i in range(len(classif)):
            if (i%2 == 1):
                y.append(classif[i])
            else:
                x.append(classif[i])

        fileOut = 'preparedModels' + str(l) + '.bin'
        res = []
        res.append(x)
        res.append(y)
        res.append(len(elemset))
        res.append(len(elemsetNonPores))
        allCoords.append(res)
        
        print('    saving results for training set, model: ',l)
        pickle.dump(res,open(fileOut,'wb'))

    return allCoords


def classifyObject(X, modelCoords):
    resultsNew = 0
    
    for l in range(getNModels()):
        model = models[l]

        classif = kohonen20x20predict(model, X)
        nx=[]
        ny=[]
        for i in range(len(classif)):
            if (i%2 == 1):
                ny.append(classif[i])
            else:
                nx.append(classif[i])

        x,y,lenElemset,lenElemsetNonPores = modelCoords[l]
        nx = nx[0]
        ny = ny[0]


        xMeanPores = np.mean(x[0:lenElemset])
        yMeanPores = np.mean(y[0:lenElemset])
        xMeanNonPores = np.mean(x[lenElemset:(lenElemset+lenElemsetNonPores)])
        yMeanNonPores = np.mean(y[lenElemset:(lenElemset+lenElemsetNonPores)])

        k = (yMeanPores - yMeanNonPores)/(xMeanPores - xMeanNonPores)

        m = (yMeanPores + yMeanNonPores)/2 + (xMeanPores + xMeanNonPores)/(2*k)
        x2 = np.array(range(20))
        y2 = np.zeros(20)
        for i in range(20):
            y2[i] = m - i/k

        
        if yMeanPores > yMeanNonPores:
            if ny>y2[int(nx)]:
                resultsNew += 1
        elif yMeanPores < yMeanNonPores:
            if ny<y2[int(nx)]:
                resultsNew += 1

    return resultsNew/getNModels()




img = skimage.io.imread(inputimage)

models = []
for l in range(getNModels()):
    modelfilename = modelsfile + str(l) +'.bin'
    models.append(pickle.load(open(modelfilename, 'rb')))

print (getNModels(),' models loaded')


#Either prepare training set results for the models,
#or load them from previous run with same models (but not necessarily same image)
if doPrepareModels:
    print('Preparing models...')
    modelCoords = prepareModels()
    print('Done')
else:
    print('Loading model preparations...')
    modelCoords = []
    for l in range(getNModels()):
        file = 'preparedModels' + str(l) + '.bin'
        res = pickle.load(open(file, 'rb'))
        modelCoords.append(res)
    print('Done')
        
print('Thresholding and morphological processing...')

binary_adaptive = threshold(img)
"""
###THIS IS THE GABOR PART

gabImgR,gabImgI = gabor(img,0.35,np.pi/2,n_stds=15)
areaThresh = gabImgR > 20
areaThresh = binary_dilation(areaThresh,disk(1))
areaThresh = np.invert(areaThresh)
areaThresh = opening(areaThresh,disk(6))
indicesWhereWhite = np.where(areaThresh)
intensityWhite = img[indicesWhereWhite[0],indicesWhereWhite[1]]
smoothedImage = np.ones(np.shape(img))*np.mean(intensityWhite)
smoothedImage[indicesWhereWhite[0],indicesWhereWhite[1]] = img[indicesWhereWhite[0],indicesWhereWhite[1]]
smoothedImage = gaussian(smoothedImage,6)
img[indicesWhereWhite[0],indicesWhereWhite[1]] = smoothedImage[indicesWhereWhite[0],indicesWhereWhite[1]]

### END OF GABOR PART
"""
print('Done')

print('Searching for blobs')

labels = label(binary_adaptive, background=0)
blobs = regionprops(labels)

print('Found', len(blobs), 'blobs')

fig, ax = plt.subplots()
plt.imshow(img, cmap='gray')

alpha = 1
found = 0

if nTimes < 1:
    nTimes = len(blobs)

final_results = np.zeros(nTimes)

for i in range(0, nTimes):
    x, y = blobs[i]['centroid']
    size = blobs[i]['equivalent_diameter']

    if((x - alpha*size) < 0 or (x + alpha*size) > img.shape[0] or (y - alpha*size) < 0 or (y + alpha*size) > img.shape[1]):
        continue

    snapshot = img[int(x-alpha*size):int(x+alpha*size), int(y-alpha*size):int(y+alpha*size)]

    mask = labels[int(x-alpha*size):int(x+alpha*size), int(y-alpha*size):int(y+alpha*size)]
    mask = mask == blobs[i]['label']
    mask = dilation(mask, disk(2))

    X = computeFeatures(snapshot, blobs[i], mask)
    Y = classifyObject(X,modelCoords)
    if printProgress:
        print('status:', i/nTimes*100,'%')
    
    final_results[i] = Y
    if(Y>percentage):
        found = found + 1
        ax.add_artist(plt.Circle((y, x), size, color=(Y,1.0-Y,0.0), fill=False))
    else:
        ax.add_artist(plt.Circle((y, x), size, color=(0.0,0.0,1.0-Y), fill=False))

print('saving results')
pickle.dump(final_results, open(resultsFile, 'wb'))

print('Red, green and between are identified as pores, blue are not')
print(found, 'objects where identified as pores (', found/nTimes*100, '%)')
plt.show()
    

