import skimage.io
from skimage.filters import threshold_local, gabor, gaussian
from skimage.measure import regionprops, label, moments_hu
from skimage.morphology import opening, disk, binary_dilation
import skimage.measure
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import pickle
import sys
import os

path = sys.argv[1]
outpath = sys.argv[2]

files = os.listdir(path)


for file in files:
    img = skimage.io.imread(path+file)
    gabImgR,gabImgI = gabor(img,0.35,np.pi/2,n_stds=15)
    areaThresh = gabImgR > 20
    areaThresh = binary_dilation(areaThresh,disk(1))
    areaThresh = np.invert(areaThresh)
    areaThresh = opening(areaThresh,disk(6))
    indicesWhereWhite = np.where(areaThresh)
    intensityWhite = img[indicesWhereWhite[0],indicesWhereWhite[1]]
    smoothedImage = np.ones(np.shape(img))*np.mean(intensityWhite)
    smoothedImage[indicesWhereWhite[0],indicesWhereWhite[1]] = img[indicesWhereWhite[0],indicesWhereWhite[1]]
    smoothedImage = gaussian(smoothedImage,6)
    img[indicesWhereWhite[0],indicesWhereWhite[1]] = smoothedImage[indicesWhereWhite[0],indicesWhereWhite[1]]

    outfilepath = outpath+str('gabor_')+file
    print('Writing', outfilepath)
    skimage.io.imsave(outfilepath, img)

