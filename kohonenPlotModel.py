import numpy as np
import pickle
import sys
import random
import skimage.measure
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
from common import getFeatureSize
from element import Element

#PARAMETERS---------------------------------
plotWithPerturbation = True

modelNumber = 1

#print what pores are not correctly classified
printErrors = False



#INPUT FILES---------------------------------
elementsetsInfo = 'ElementSetsInfo.bin'

#First part of filenames: (number + .bin is added)
#pores in test set
poresResultsFile = 'resultsNewPores'
#non-pores in test set
nonPoresResultsFile = 'resultsNewNonPores'
#training set
trainingsetResultsFile = 'resultsTrainingset'




#------------------------------------------


file = trainingsetResultsFile+str(modelNumber)+'.bin'
nFile = poresResultsFile+str(modelNumber)+'.bin'
nnFile = nonPoresResultsFile+str(modelNumber)+'.bin'

x,y = pickle.load(open(file, 'rb'))

#new pores
nx,ny = pickle.load(open(nFile, 'rb'))

#new non-pores
nnx,nny = pickle.load(open(nnFile, 'rb'))

lenElemsetNew, lenElemsetNewNon,lenElemsetPores,lenElemsetNonPores = pickle.load(open('ElementSetsInfo.bin', 'rb'))

xMeanPores = np.mean(x[0:lenElemsetPores])
yMeanPores = np.mean(y[0:lenElemsetPores])
xMeanNonPores = np.mean(x[lenElemsetPores:(lenElemsetPores+lenElemsetNonPores)])
yMeanNonPores = np.mean(y[lenElemsetPores:(lenElemsetPores+lenElemsetNonPores)])
xMeanNew = np.mean(nx)
yMeanNew = np.mean(ny)
xMeanNewNon = np.mean(nnx)
yMeanNewNon = np.mean(nny)


k = (yMeanPores - yMeanNonPores)/(xMeanPores - xMeanNonPores)


m = (yMeanPores + yMeanNonPores)/2 + (xMeanPores + xMeanNonPores)/(2*k)
x2 = np.array(range(20))
y2 = np.zeros(20)
for i in range(20):
    y2[i] = m - i/k

sucessratePores = 0
sucessrateNonPores = 0
sucessrateNew = 0
sucessrateNewNon = 0


#new pores
if printErrors:
    print('Not correctly classified pores(test set): ')
for i in range(lenElemsetNew):
    if yMeanPores > yMeanNonPores:
        if ny[i]>y2[int(nx[i])]:
            sucessrateNew += 1
        else:
            if printErrors :
                print(i)
    elif yMeanPores < yMeanNonPores:
        if ny[i]<y2[int(nx[i])]:
            sucessrateNew += 1
        else:
            if printErrors :
                print(i)
#new non-pores
if printErrors:
    print('Not correctly classified non-pores(test set): ')
for i in range(lenElemsetNewNon):
    if yMeanPores > yMeanNonPores:
        if nny[i]<y2[int(nnx[i])]:
            sucessrateNewNon += 1
        else:
            if printErrors :
                print(i)
    elif yMeanPores < yMeanNonPores:
        if nny[i]>y2[int(nnx[i])]:
            sucessrateNewNon += 1
        else:
            if printErrors :
                print(i)

                
if printErrors:
    print('Not correctly classified pores(training set): ')
    
for i in range(lenElemsetPores):
    if yMeanPores > yMeanNonPores:
        if y[i]>y2[int(x[i])]:
            sucessratePores += 1
        else:
            if printErrors :
                print(i)
    elif yMeanPores < yMeanNonPores:
        if y[i]<y2[int(x[i])]:
            sucessratePores += 1
        else:
            if printErrors :
                print(i)


if printErrors:
    print('Not correctly classified  non-pores(training set): ')
    
for i in range(lenElemsetNonPores):
    if yMeanNonPores > yMeanPores:
        if y[i+152]>y2[int(x[i+152])]:
            sucessrateNonPores += 1
        else:
            if printErrors :
                print(i)
    elif yMeanNonPores < yMeanPores:
        if y[i+152]<y2[int(x[i+152])]:
            sucessrateNonPores += 1
        else:
            if printErrors :
                print(i)

print('Successrate pores(model): ',sucessratePores/lenElemsetPores*100,'%')
print('Successrate non-pores(model): ',sucessrateNonPores/lenElemsetNonPores*100,'%')
print('Succesrate New pores: ',sucessrateNew/lenElemsetNew*100,'%')
print('Succesrate New non-pores: ',sucessrateNewNon/lenElemsetNewNon*100,'%')


if plotWithPerturbation:
    for i in range((lenElemsetPores+lenElemsetNonPores)):
        x[i] += random.random()-0.5
        y[i] += random.random()-0.5
    for i in range(lenElemsetNew):
        nx[i] += random.random()-0.5
        ny[i] += random.random()-0.5
    for i in range(lenElemsetNewNon):
        nnx[i] += random.random()-0.5
        nny[i] += random.random()-0.5
        
    


red_dots = mlines.Line2D([], [], color='red', marker='o',
                          markersize=10, label='Pore Elements(model)')
red_cross = mlines.Line2D([], [], color='red', marker='x',
                          markersize=10, label='Mean position of pore elements(model)')



green_dots = mlines.Line2D([], [], color='green', marker='o',
                          markersize=10, label='Non-Pore Elements(model)')
green_cross = mlines.Line2D([], [], color='green', marker='x',
                          markersize=10, label='Mean position of non-pore elements(model)')

blue_dots = mlines.Line2D([], [], color='blue', marker='o',
                          markersize=10, label='Test pore elements')
blue_cross = mlines.Line2D([], [], color='blue', marker='x',
                          markersize=10, label='Mean position of test pore elements')

yellow_dots = mlines.Line2D([], [], color='yellow', marker='o',
                          markersize=10, label='Test non-pore elements')
yellow_cross = mlines.Line2D([], [], color='yellow', marker='x',
                      markersize=10, label='Mean position of test non-pore elements')


plt.legend(handles=[red_dots, red_cross, green_dots, green_cross, blue_dots, blue_cross, yellow_dots, yellow_cross])



plt.axis([0,20,0,20])
ax = plt.gca()
ax.set_autoscale_on(False)

plt.xlabel('Output neurons, first dimension')
plt.ylabel('Output neurons, second dimension')

plt.xticks(range(20))
plt.yticks(range(20))

#plt.plot(x[0:lenElemset],y[0:lenElemset],'ro', xmean,ymean,'bx')

plt.plot(xMeanPores,yMeanPores,'rx',xMeanNonPores,yMeanNonPores,'gx',xMeanNew,yMeanNew,'bx',xMeanNewNon,yMeanNewNon,'yx')
plt.plot(x[0:lenElemsetPores],y[0:lenElemsetPores],'ro',x[lenElemsetPores:(lenElemsetPores+lenElemsetNonPores)],y[lenElemsetPores:(lenElemsetPores+lenElemsetNonPores)],'go')
plt.plot(nx,ny,'bo',nnx,nny,'yo')
plt.plot(x2,y2,'purple')

plt.show()


