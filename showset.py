import pickle
import numpy as np
from skimage.morphology import dilation, disk
import matplotlib.pyplot as plt
import sys

if(len(sys.argv) == 1):
    print('Function:')
    print('Display the data in an element set')
    print('Usage:')
    print('showset.py filename')
    quit()

filename = sys.argv[1]

print('Loading', filename)
elemset = pickle.load(open(filename, 'rb'))
print(len(elemset), 'elements found')

for i in range(0, int(np.ceil(len(elemset)/16))):
    a = 16
    if((i+1)*16 >= len(elemset)):
        a = len(elemset)%16

    for j in range(0, a):
        plt.subplot(4, 4, j+1)
        curelem = elemset[i*16+j]
        contour = dilation(curelem.mask, disk(1)) - curelem.mask
        contour = np.ma.masked_where(contour == False, contour)
        plt.imshow(curelem.snapshot, cmap='gray', vmin=0, vmax=255, interpolation='bicubic')
        plt.imshow(contour, interpolation='none')


    plt.show()

plt.show()
