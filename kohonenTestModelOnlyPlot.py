import skimage.io
from skimage.filters import threshold_local, gabor, gaussian
from skimage.measure import regionprops, label, moments_hu
from skimage.morphology import opening, disk, dilation, binary_dilation
from common import threshold, computeFeatures,getNModels,getFeatureSize
import skimage.measure
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import pickle
import sys

#This is for all blobs found in one picture

inputimage = 'sample2_final_grey_image173.tif'

final_results = pickle.load(open('final_results.bin', 'rb'))

#Percentage of models classifying an object needed to consider it a pore
percentage = 0.08

#----------------------------------

img = skimage.io.imread(inputimage)



print('Thresholding and morphological processing...')

binary_adaptive = threshold(img)
"""
###THIS IS THE GABOR PART

gabImgR,gabImgI = gabor(img,0.35,np.pi/2,n_stds=15)
areaThresh = gabImgR > 20
areaThresh = binary_dilation(areaThresh,disk(1))
areaThresh = np.invert(areaThresh)
areaThresh = opening(areaThresh,disk(6))
indicesWhereWhite = np.where(areaThresh)
intensityWhite = img[indicesWhereWhite[0],indicesWhereWhite[1]]
smoothedImage = np.ones(np.shape(img))*np.mean(intensityWhite)
smoothedImage[indicesWhereWhite[0],indicesWhereWhite[1]] = img[indicesWhereWhite[0],indicesWhereWhite[1]]
smoothedImage = gaussian(smoothedImage,6)
img[indicesWhereWhite[0],indicesWhereWhite[1]] = smoothedImage[indicesWhereWhite[0],indicesWhereWhite[1]]

### END OF GABOR PART
"""
print('Done')

print('Searching for blobs')

labels = label(binary_adaptive, background=0)
blobs = regionprops(labels)

print('Found', len(blobs), 'blobs')

fig, ax = plt.subplots()
plt.imshow(img, cmap='gray')

alpha = 1
found = 0
nTimes = len(blobs)



for i in range(0, nTimes):
    x, y = blobs[i]['centroid']
    size = blobs[i]['equivalent_diameter']

    if((x - alpha*size) < 0 or (x + alpha*size) > img.shape[0] or (y - alpha*size) < 0 or (y + alpha*size) > img.shape[1]):
        continue

    snapshot = img[int(x-alpha*size):int(x+alpha*size), int(y-alpha*size):int(y+alpha*size)]

    mask = labels[int(x-alpha*size):int(x+alpha*size), int(y-alpha*size):int(y+alpha*size)]
    mask = mask == blobs[i]['label']
    mask = dilation(mask, disk(2))


    if (i%10==0):
        print('status:', i/nTimes*100,'%')
    Y = final_results[i]

    if(Y>percentage):
        found = found + 1
        ax.add_artist(plt.Circle((y, x), size, color=(Y,1.0-Y,0.0), fill=False))
    else:
        ax.add_artist(plt.Circle((y, x), size, color=(0.0,0.0,1.0-Y), fill=False))


print('Red, green and between are identified as pores, blue are not')
print(found, 'objects where identified as pores (', found/nTimes*100, '%)')
plt.show()
    

