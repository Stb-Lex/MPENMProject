import numpy as np
import pickle
import sys
import random
import skimage.measure
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
from common import getFeatureSize
from common import getNModels
from element import Element


"""
See main function for parameters

kohonen20x20predict(...) takes model and data to sort as arguments. It gives classifications in the
output grid. 

"""

#sorting the input according to network's classification
def kohonen20x20predict(model, data):
    weights = model[0]
    dim = model[1]
    means = model[2]
    stdevs = model[3]
    
    inpt = np.zeros(len(data))
    for i in range(len(inpt)):
        inpt[i] = data[i]

    n_inpt = int(len(data)//dim)


    for j in range(dim):
        for i in range(n_inpt):
            inpt[i*dim + j] = (inpt[i*dim + j] - means[j])/stdevs[j]
    
    classifications = np.zeros(n_inpt*2)
    
    i0=0
    for j in range(n_inpt):
        largest = -10000.0
        for i in range(400):
            buffer = 0.0
            for k in range(dim):
                buffer += weights[i*dim + k]*inpt[j*dim + k]
            if (buffer > largest):
                largest = buffer
                i0 = i
        classifications[j*2] = int(i0//20)
        classifications[j*2+1] = int(i0%20)

    return classifications


def main(argv):
    #INPUT FILES
    elemsetfilename = 'p'
    elemsetNonPoresFilename = 'np'
    testSetPores = 'c'
    testSetNonPores = 'cn'

    #first part of the model file names. They are loaded as 'modelsfilename'+ a number + '.bin'
    modelsfile = 'model'

    #OUTPUT FILES
    elementsetsFile = 'ElementSetsInfo.bin'

    #to these are appended a number + .bin
    poresOutFile = 'resultsNewPores'
    nonPoresOutFile = 'resultsNewNonPores'
    trainingsetOutFile = 'resultsTrainingset'
    



    print('Loading pore training set')
    elemset = pickle.load(open(elemsetfilename, 'rb'))
    print('Done')

    print('Loading non-pore training set')
    elemsetNonPores = pickle.load(open(elemsetNonPoresFilename, 'rb'))
    print('Done')

    print('Loading pore set to be classified')
    elemsetNewPores = pickle.load(open(testSetPores,'rb'))
    print('Done')

    print('Loading non-pore set to be classified')
    elemsetNewNonPores = pickle.load(open(testSetNonPores,'rb'))
    print('Done')
    



    X = np.zeros([len(elemset),getFeatureSize()])
    for i in range(len(elemset)):
        X[i,:] = elemset[i].features

    
    X2 = np.zeros([len(elemsetNonPores),getFeatureSize()])
    for i in range(len(elemsetNonPores)):
        X2[i,:] = elemsetNonPores[i].features


    cX = np.zeros([len(elemsetNewPores),getFeatureSize()])
    for i in range(len(elemsetNewPores)):
        cX[i,:] = elemsetNewPores[i].features
        
    cnX = np.zeros([len(elemsetNewNonPores),getFeatureSize()])
    for i in range(len(elemsetNewNonPores)):
        cnX[i,:] = elemsetNewNonPores[i].features
    
        
    print('Rearranging data to fit Kohonen function requirements')

    Y = np.zeros((len(elemset)+len(elemsetNonPores))*getFeatureSize())
    for i in range(len(elemset)):
        for j in range(getFeatureSize()):
            Y[i*getFeatureSize()+j]=X[i,j]
        
    lenElemset = len(elemset)
    print('Number of pore elements: ', lenElemset)
    print('Number of non-pore elements: ',len(elemsetNonPores))
    
    for i in range(len(elemsetNonPores)):
        for j in range(getFeatureSize()):
            Y[(lenElemset+i)*getFeatureSize() + j] = X2[i,j]



    cY = np.zeros(len(elemsetNewPores)*getFeatureSize())
    for i in range(len(elemsetNewPores)):
        for j in range(getFeatureSize()):
            cY[i*getFeatureSize()+j]=cX[i,j]

    cnY = np.zeros(len(elemsetNewNonPores)*getFeatureSize())
    for i in range(len(elemsetNewNonPores)):
        for j in range(getFeatureSize()):
            cnY[i*getFeatureSize()+j]=cnX[i,j]

    print('Saving element set sizes')
    setsInfo =[]
    setsInfo.append(len(elemsetNewPores))
    setsInfo.append(len(elemsetNewNonPores))
    setsInfo.append(len(elemset))
    setsInfo.append(len(elemsetNonPores))
    pickle.dump(setsInfo,open(elementsetsFile,'wb'))
    print('Done')
    
    print('Classifying:')

    for l in range(getNModels()):
        fileModel = modelsfile + str(l) + '.bin'
        model = pickle.load(open(fileModel, 'rb'))

        #new set pores
        classif = kohonen20x20predict(model, cY)
        x=[]
        y=[]
        for i in range(len(classif)):
            if (i%2 == 1):
                y.append(classif[i])
            else:
                x.append(classif[i])

        fileOut = poresOutFile + str(l) + '.bin'
        res = []
        res.append(x)
        res.append(y)
        print('saving results for new pores, model: ',l)
        pickle.dump(res,open(fileOut,'wb'))

        #new set non-pores
        classif = kohonen20x20predict(model, cnY)
        x=[]
        y=[]
        for i in range(len(classif)):
            if (i%2 == 1):
                y.append(classif[i])
            else:
                x.append(classif[i])

        fileOut = nonPoresOutFile + str(l) + '.bin'
        res = []
        res.append(x)
        res.append(y)
        print('saving results for new non-pores, model: ',l)
        pickle.dump(res,open(fileOut,'wb'))
        
        #training set
        classif = kohonen20x20predict(model, Y)

        x=[]
        y=[]
        for i in range(len(classif)):
            if (i%2 == 1):
                y.append(classif[i])
            else:
                x.append(classif[i])

        fileOut = trainingsetOutFile + str(l) + '.bin'
        res = []
        res.append(x)
        res.append(y)

        print('saving results for training set, model: ',l)
        pickle.dump(res,open(fileOut,'wb'))


if __name__ == "__main__":
    main(sys.argv)
