import skimage.io
from skimage.filters import threshold_local, gabor, gaussian
from skimage.measure import regionprops, label, moments_hu
from skimage.morphology import opening, disk, dilation, binary_dilation
from common import threshold, computeFeatures
import skimage.measure
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import pickle
import sys

negthresh = 128

modelfilename = str(sys.argv[1])
inputimage = str(sys.argv[2])

img = skimage.io.imread(inputimage)

print('Loading model')
model = pickle.load(open(modelfilename, 'rb'))
print('Done')

print('Thresholding and morphological processing...')

binary_adaptive = threshold(img)

print('Done')

print('Searching for blobs')

labels = label(binary_adaptive, background=0)
blobs = regionprops(labels)

print('Found', len(blobs), 'blobs')

out = np.zeros(img.shape)

alpha = 1
found = 0
for i in range(0, len(blobs)):
    x, y = blobs[i]['centroid']
    size = blobs[i]['equivalent_diameter']

    if((x - alpha*size) < 0 or (x + alpha*size) > img.shape[0] or (y - alpha*size) < 0 or (y + alpha*size) > img.shape[1]):
        continue

    snapshot = img[int(x-alpha*size):int(x+alpha*size), int(y-alpha*size):int(y+alpha*size)]

    mask = labels[int(x-alpha*size):int(x+alpha*size), int(y-alpha*size):int(y+alpha*size)]
    mask = mask == blobs[i]['label']
    mask = dilation(mask, disk(2))

    X = computeFeatures(snapshot, blobs[i], mask)
    Y = model.predict(X.reshape(1,-1))

    if(Y == 1):
        found = found + 1
        out[int(x-alpha*size):int(x+alpha*size), int(y-alpha*size):int(y+alpha*size)] = np.maximum((255-int(X[1]))*mask, out[int(x-alpha*size):int(x+alpha*size), int(y-alpha*size):int(y+alpha*size)])
    else:
        out[int(x-alpha*size):int(x+alpha*size), int(y-alpha*size):int(y+alpha*size)] = np.maximum(np.max([0, negthresh-int(X[1])])*mask, out[int(x-alpha*size):int(x+alpha*size), int(y-alpha*size):int(y+alpha*size)])



print(found, 'objects where identified as pores (', found/len(blobs)*100, '%)')

fig, ax = plt.subplots()
plt.imshow(out, cmap='jet')
plt.show()
    

