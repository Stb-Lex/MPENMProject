import skimage.io
from skimage.filters import threshold_local, try_all_threshold
from skimage.measure import regionprops, label, moments_hu, moments_normalized
from skimage.morphology import opening, dilation, disk
from element import Element
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.cm as cm
from mahotas.features import zernike_moments
from common import threshold, computeFeatures
import pickle
import sys
import os

#Same as buildset.py, except non-pore objects are also saved

if(len(sys.argv) == 1):
    print('Function:')
    print('Build an element set from a starting image')
    print('Usage:')
    quit()

outfilename = str(sys.argv[1])
outfilenameNonPores = str(sys.argv[2])


files = os.listdir(sys.argv[3])
fileIdx = np.random.randint(0, len(files))

infilename = str(sys.argv[3]) + files[fileIdx]

print('Loading file', infilename)

img = skimage.io.imread(infilename)

print('Thresholding and morphological processing...')

avg = np.mean(img.flatten())
med = np.median(img.flatten())
avgmed = (avg+med)/2
print(avg, med, avgmed)

binary_adaptive = threshold(img)
plt.imshow(binary_adaptive, cmap='gray')
plt.show()


print('Done')

print('Searching for blobs')

labels = label(binary_adaptive, background=0)
blobs = regionprops(labels)

print('Found', len(blobs), 'blobs')

elemset = []
elemsetNonPores = []

alpha = 3
cont = True
print(img.shape)
while(cont):
    idx = np.random.randint(0, len(blobs))
    x, y = blobs[idx]['centroid']
    size = blobs[idx]['equivalent_diameter']
    print('found', x, y, size)

    if((x - alpha*size) < 0 or (x + alpha*size) > img.shape[0] or (y - alpha*size) < 0 or (y + alpha*size) > img.shape[1]):
        continue

    snapshot = img[int(x-alpha*size):int(x+alpha*size), int(y-alpha*size):int(y+alpha*size)]

    fig, ax = plt.subplots()

    mask = labels[int(x-alpha*size):int(x+alpha*size), int(y-alpha*size):int(y+alpha*size)]
    mask = mask == blobs[idx]['label']
    mask = dilation(mask, disk(2))

    contour = dilation(mask, disk(1)) - mask
    contour = np.ma.masked_where(contour == False, contour)
    
    plt.imshow(snapshot, vmin=0, vmax=255, cmap='gray', interpolation='bicubic')
    plt.imshow(contour, interpolation='none')
    
    plt.show()

    answer = input(str(str(len(elemset)) +',' + str(len(elemsetNonPores)) + ': Valid pore (y), invalid (n), quit (q):'))
    if(answer == 'y'):
        elemset.append(Element(snapshot, mask, blobs[idx], computeFeatures(snapshot, blobs[idx], mask)))
    elif(answer == 'n'):
        elemsetNonPores.append(Element(snapshot, mask, blobs[idx], computeFeatures(snapshot, blobs[idx], mask)))
    elif(answer == 'q'):
        cont = False

print('Saving collected pore elements to', outfilename)
pickle.dump(elemset, open(outfilename, 'wb'))

print('Saving collected non-pore elements to', outfilenameNonPores)
pickle.dump(elemsetNonPores, open(outfilenameNonPores, 'wb'))

print('Done.', len(elemset), 'pore elements have been saved.')

print('Done.', len(elemsetNonPores), 'non pore elements have been saved.')


