import numpy as np
import pickle
import sys
import random
import skimage.measure
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
from common import getFeatureSize
from common import getNModels
from element import Element


#INPUT FILES---------------------------------
elementsetsInfo = 'ElementSetsInfo.bin'

#First part of filenames: (number + .bin is added)
#pores in test set
poresResultsFile = 'resultsNewPores'
#non-pores in test set
nonPoresResultsFile = 'resultsNewNonPores'
#training set
trainingsetResultsFile = 'resultsTrainingset'

#number of models can be changed in common.py


lenElemsetNew, lenElemsetNewNon,lenElemsetPores,lenElemsetNonPores = pickle.load(open(elementsetsInfo, 'rb'))

results = np.zeros((lenElemsetPores+lenElemsetNonPores))
resultsNew = np.zeros(lenElemsetNew)
resultsNewNon = np.zeros(lenElemsetNewNon)

for l in range(getNModels()):
    file = trainingsetResultsFile + str(l) + '.bin'
    filen = poresResultsFile + str(l) + '.bin'
    filenn = nonPoresResultsFile + str(l) + '.bin'

    x,y = pickle.load(open(file, 'rb'))
    nx,ny = pickle.load(open(filen, 'rb'))
    nnx,nny = pickle.load(open(filenn, 'rb'))

    xMeanPores = np.mean(x[0:lenElemsetPores])
    yMeanPores = np.mean(y[0:lenElemsetPores])
    xMeanNonPores = np.mean(x[lenElemsetPores:(lenElemsetPores+lenElemsetNonPores)])
    yMeanNonPores = np.mean(y[lenElemsetPores:(lenElemsetPores+lenElemsetNonPores)])
    xMeanNew = np.mean(nx)
    yMeanNew = np.mean(ny)
    xMeanNewNon = np.mean(nnx)
    yMeanNewNon = np.mean(nny)
    
    #Create line separating pores and non-pores
    k = (yMeanPores - yMeanNonPores)/(xMeanPores - xMeanNonPores)


    m = (yMeanPores + yMeanNonPores)/2 + (xMeanPores + xMeanNonPores)/(2*k)
    x2 = np.array(range(20))
    y2 = np.zeros(20)
    for i in range(20):
        y2[i] = m - i/k



    #Check if classified as pore:
    
    #training set
    for i in range((lenElemsetPores+lenElemsetNonPores)):
        if yMeanPores > yMeanNonPores:
            if y[i]>y2[int(x[i])]:
                results[i] += 1
        elif yMeanPores < yMeanNonPores:
            if y[i]<y2[int(x[i])]:
                results[i] += 1


    #new pores
    for i in range(lenElemsetNew):
        if yMeanPores > yMeanNonPores:
            if ny[i]>y2[int(nx[i])]:
                resultsNew[i] += 1
        elif yMeanPores < yMeanNonPores:
            if ny[i]<y2[int(nx[i])]:
                resultsNew[i] += 1

    #new non-pores
    for i in range(lenElemsetNewNon):
        if yMeanPores > yMeanNonPores:
            if nny[i]>y2[int(nnx[i])]:
                resultsNewNon[i] += 1
        elif yMeanPores < yMeanNonPores:
            if nny[i]<y2[int(nnx[i])]:
                resultsNewNon[i] += 1
    
    
                

srp = np.zeros(getNModels())
srnp = np.zeros(getNModels())
srnew = np.zeros(getNModels())
srnewnon = np.zeros(getNModels())


for j in range(getNModels()):
    
    succesratePores = 0
    succesrateNonPores = 0
    succesrateNew = 0
    succesrateNewNon = 0
    
    for i in range(lenElemsetPores):
        if results[i]>j:
            succesratePores += 1

    for i in range(lenElemsetNonPores):
        if results[i+152]<j+1:
            succesrateNonPores += 1
            
    for i in range(lenElemsetNew):
        if resultsNew[i]>j:
            succesrateNew += 1

    for i in range(lenElemsetNewNon):
        if resultsNewNon[i]<j+1:
            succesrateNewNon += 1
            
    
    srp[j] = succesratePores/lenElemsetPores
    srnp[j] = succesrateNonPores/lenElemsetNonPores
    srnew[j] = succesrateNew/lenElemsetNew
    srnewnon[j] = succesrateNewNon/lenElemsetNewNon

plt.xlabel('#times registered as a pore necessary to count as a pore')
plt.ylabel('Correctly classified (%)')

plt.xticks(range(20))

line1, = plt.plot(range(1,getNModels()+1),srp,'blue',label='Pores(model)')
line2, = plt.plot(range(1,getNModels()+1),srnp,'red',label='Non-Pores(model)')
line3, = plt.plot(range(1,getNModels()+1),srnew,'black',label='Test Pores')
line4, = plt.plot(range(1,getNModels()+1),srnewnon,'yellow',label='Test Non-Pores')

plt.legend(handles=[line1, line2, line3, line4])
plt.show()

