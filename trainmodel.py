import numpy as np
import pickle
import sys
import skimage.measure
import matplotlib.pyplot as plt
from common import getFeatureSize
from sklearn import svm


elemsetfilename = sys.argv[1]
outfilename = sys.argv[2]

print('Loading training set')
elemset = pickle.load(open(elemsetfilename, 'rb'))
print('Done')

X = np.zeros([len(elemset),getFeatureSize()])
for i in range(0, len(elemset)):
    X[i,:] = elemset[i].features

print('Training model')
model = svm.OneClassSVM(nu=0.001, kernel="rbf", gamma=0.0001, tol=0.0001)
model.fit(X)

print('Done')
pickle.dump(model, open(outfilename, 'wb'))
print('Saving model')



