import numpy as np
from skimage.filters import threshold_local
from skimage.morphology import opening, disk
from mahotas.features import zernike_moments, haralick


def threshold(img):
    block_size = 21
    adaptive_thresh = threshold_local(img, block_size, offset=5)
    binary_adaptive = img > adaptive_thresh
    binary_adaptive = np.invert(binary_adaptive)
    binary_adaptive = opening(binary_adaptive, disk(1))
    binary_adaptive = opening(binary_adaptive, disk(1)).astype(int)

    return binary_adaptive

def getFeatureSize():
    #return 42 # nice
    #return 13
    return 29

#Number of Kohonen networks to use
def getNModels():
    return 20

def computeFeatures(snapshot, regionprop, mask):
    masked = np.zeros(mask.shape)
    np.copyto(masked, snapshot, where=mask)

    masked = masked[~(masked==0).all(1)]
    masked = masked[:,~(masked==0).all(0)]

    f = masked.flatten()

    # We compute the set of features
    # 0 - number of pixels
    # 1 - mean color of the pore
    # 2 - variance color of the pore
    # 3 - size (radius) of the pore
    # 4-29 - 25 first Zernike moments
    feat = np.zeros(getFeatureSize())
    feat[0] = np.sum(mask)
    feat[1] = np.sum(masked)/feat[0]
    feat[2] = np.var(f[f!=0])
    feat[3] = regionprop['equivalent_diameter']
    feat[4:29] = zernike_moments(masked, feat[2])
    #feat[29:42] = haralick(masked.astype(int)).mean(0)

    #feat = haralick(masked.astype(int)).mean(0)

    return feat
