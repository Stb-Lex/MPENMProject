import skimage.io
from skimage.filters import threshold_local, gabor, gaussian
from skimage.measure import regionprops, label, moments_hu
from skimage.morphology import opening, disk, dilation, binary_dilation, convex_hull_image
from common import threshold, computeFeatures
import skimage.measure
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import pickle
import sys


inputimage = str(sys.argv[1])

img = skimage.io.imread(inputimage)

print('Thresholding and morphological processing...')

binary_adaptive = threshold(img)

print('Done')

print('Searching for objects')

labels = label(binary_adaptive, background=0)
blobs = regionprops(labels)

print('Found', len(blobs), 'objects')

out = np.zeros(img.shape)

alpha = 1
found = 0
for i in range(0, len(blobs)):
    x, y = blobs[i]['centroid']
    size = blobs[i]['equivalent_diameter']

    if((x - alpha*size) < 0 or (x + alpha*size) > img.shape[0] or (y - alpha*size) < 0 or (y + alpha*size) > img.shape[1]):
        continue

    snapshot = img[int(x-alpha*size):int(x+alpha*size), int(y-alpha*size):int(y+alpha*size)]

    mask = labels[int(x-alpha*size):int(x+alpha*size), int(y-alpha*size):int(y+alpha*size)]
    mask = mask == blobs[i]['label']
    mask = convex_hull_image(mask)
    mask = dilation(mask, disk(2))

    masked = np.zeros(mask.shape)
    np.copyto(masked, snapshot, where=mask)

    masked = masked[~(masked==0).all(1)]
    masked = masked[:,~(masked==0).all(0)]
    
    m = np.sum(masked)/np.sum(mask)

    out[int(x-alpha*size):int(x+alpha*size), int(y-alpha*size):int(y+alpha*size)] = np.maximum((255-int(m))*mask, out[int(x-alpha*size):int(x+alpha*size), int(y-alpha*size):int(y+alpha*size)])


fig, ax = plt.subplots()
plt.imshow(out, cmap='jet')
plt.show()
    
