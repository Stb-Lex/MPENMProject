import cython
import skimage.io
from skimage.filters import threshold_local
from skimage.measure import regionprops, label, moments_hu
from skimage.morphology import opening, disk, dilation, erosion
import skimage.measure
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import pickle
import sys
#Should probably be remade into a function that takes an image as input and outputs the gradient on the pores

gradientFunc = lambda x: 1-np.exp(-x) #The logistic function
totalGradientWidth = 5 #How deep into the logistic function you want to go

#START of part that can be removed if made into a function
img = skimage.io.imread('data\sample2_final_grey_image767.tif')
block_size = 51
adaptive_thresh = threshold_local(img, block_size, offset=20)
binary_adaptive = img > adaptive_thresh
binary_adaptive = np.invert(binary_adaptive)
binary_adaptive = opening(binary_adaptive, disk(1))
binary_adaptive = opening(binary_adaptive, disk(1)).astype(int)

#END of part which can be removed..
labels = label(binary_adaptive, background=0)
blobs = regionprops(labels)
store_img_final = np.zeros(img.shape) #This contains the final gradients
#This part can probably be improved
for k in range(0,len(blobs)):
    store_img = np.zeros(img.shape,dtype = np.uint8) #Create the image for dilation, should probably be made smaller
    store_img2 = np.zeros(img.shape,dtype = np.uint8) #The image for the difference between the images
    ind = blobs[k]['coords'] #Coordinates
    rowStore = []
    colStore = []
    for i in range(0,len(ind)): #Store index pos for the objects
        rowStore.append(ind[i][0])
        colStore.append(ind[i][1])
    store_img[rowStore,colStore] = binary_adaptive[rowStore,colStore]
    store_img = dilation(store_img,disk(1)) #Dilation to so the gradient starts outside pore
    #THE GRADIENT DEPENDS ON STRUCTURE ELEMENT FOR ERODE AND DILATE SO MAKE SURE THEY ARE GOOD, maybe square works better than circle....
    erosionImage = store_img
    numberOfErosions =0
    while np.any(erosionImage): #Number of erosions until object is gone
        erosionImage = erosion(erosionImage,disk(1))
        numberOfErosions +=1

    stepNumber = 1
    while np.any(store_img): # Erode and put logistic function on perimeter each time
        store_img2 = store_img 
        store_img = erosion(store_img2,disk(1))
        store_img2 = np.subtract(store_img2,store_img)
        row,col = np.where(store_img2==1)
        store_img_final[row,col] = gradientFunc(stepNumber*totalGradientWidth/numberOfErosions)
        stepNumber +=1


plt.imshow(store_img_final, cmap='gray')
plt.show()

